import QtQuick 2.1
import QtQuick.Controls.Material 2.1
import QtQuick.Controls 2.1
Item {
    id: root

    property int vt: 340
    property int peep: 10
    property int oxy: 21
    width: 600
    height: 480
    rotation: 0

    Row {
        width: 600
        height: 480
        anchors.fill: parent
        anchors.leftMargin: -58
        anchors.topMargin: 62
        anchors.rightMargin: -54
        anchors.bottomMargin: 64
        spacing: 22

        Image {
            width: 86
            height: 232
            source: "qrc:/images/current-mode.png"
            anchors.verticalCenter: parent.verticalCenter
        }

        Item {
            width: 90
            height: 270
            Column {
                anchors.fill: parent
                anchors.leftMargin: 5

                Button {
                    id: btnVTPlus
                    width: parent.width
                    height: 70
                    contentItem: Image {
                        source: btnVTPlus.pressed ?
                           "qrc:/images/btn-add-down.png":
                           "qrc:/images/btn-add-normal.png"

                    }
                    onClicked: root.vt++;
                }

                Item {
                    id: vt
                    width: parent.width
                    height: 130

                    Text {
                        text: root.vt.toString()
                        color: "#5BCEBD"
                        font.pixelSize: 50
                        x: 23
                        y: 4
                        transform: [
                            Rotation {
                                origin.x: vt.width / 2
                                origin.y: vt.height / 2
                                angle: -90
                            }
                        ]
                    }
                    Image {
                        y: 56
                        x: -10
                        source: "qrc:/images/Vt-title.png"
                    }
                }

                Button {
                    id: btnVTMinus
                    width: parent.width
                    height: 70
                    contentItem: Image {
                        source: btnVTMinus.pressed ?
                            "qrc:/images/btn-remove-down.png":
                            "qrc:/images/btn-remove-normal.png"

                    }
                    onClicked: root.vt--;
                }
            }
        }

        Item {
            width: 90
            height: 270
            Column {
                anchors.fill: parent
                anchors.leftMargin: 5

                Button {
                    id: peepPlus
                    width: parent.width
                    height: 70
                    contentItem: Image {
                        source: peepPlus.pressed ?
                            "qrc:/images/btn-add-down.png":
                            "qrc:/images/btn-add-normal.png"

                    }
                    onClicked: root.peep++;
                }

                Item {
                    id: peep
                    width: parent.width
                    height: 130

                    Text {
                        text: root.peep.toString()
                        color: "#5BCEBD"
                        font.pixelSize: 50
                        x: 23
                        y: -15
                        transform: [
                            Rotation {
                                origin.x: peep.width / 2
                                origin.y: peep.height / 2
                                angle: -90
                            }
                        ]
                    }
                    Image {
                        y: 20
                        x: -10
                        source: "qrc:/images/peepcpap-title.png"
                    }
                }

                Button {
                    id: peepMinus
                    width: parent.width
                    height: 70
                    contentItem: Image {
                        source: peepMinus.pressed ?
                            "qrc:/images/btn-remove-down.png":
                            "qrc:/images/btn-remove-normal.png"

                    }
                    onClicked: root.peep--;
                }
            }
        }
        Item {
            width: 90
            height: 270
            Column {
                anchors.fill: parent
                anchors.leftMargin: 5

                Button {
                    id: btnOxyPlus
                    width: parent.width
                    height: 70
                    contentItem: Image {
                        source: btnOxyPlus.pressed ?
                            "qrc:/images/btn-add-down.png" :
                            "qrc:/images/btn-add-normal.png"

                    }
                    onClicked: root.oxy++;
                }

                Item {
                    id: oxy
                    width: parent.width
                    height: 130

                    Text {
                        text: root.oxy.toString()
                        color: "#5BCEBD"
                        font.pixelSize: 50
                        x: 23
                        y: -15
                        transform: [
                            Rotation {
                                origin.x: oxy.width / 2
                                origin.y: oxy.height / 2
                                angle: -90
                            }
                        ]
                    }
                    Image {
                        y: 42
                        x: -10
                        source: "qrc:/images/Oxygen-title.png"
                    }
                }

                Button {
                    id: btnOxyMinus
                    width: parent.width
                    height: 70
                    contentItem: Image {
                        source: btnOxyMinus.pressed ?
                            "qrc:/images/btn-remove-down.png" :
                            "qrc:/images/btn-remove-normal.png"

                    }
                    onClicked: root.oxy--;
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.5}
}
##^##*/
