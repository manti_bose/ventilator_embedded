import QtQuick 2.1
import QtQuick.Controls.Material 2.1
import QtQuick.Controls 2.1
Item {
    id: root
    width: 600
    height: 600

    property real xScale: 1
    property real yScale: 1
    property real sign: 1

    property real threshold: 0.07
    property real lungsOpacity: 1
    property real opacityStep: 0.1

    Timer {
        id: lungsScaler
        running: true
        repeat: true
        interval: 33

        onTriggered: {
            root.xScale += sign * 0.001
            root.yScale += sign * 0.001

            if (root.xScale > 1 + threshold) {
                sign = -1
                // opacity decreases
            }

            if (root.xScale < 1 - threshold) {
                sign = 1
                // opacity increases
            }

            lungsOpacity = 4.28571 * root.xScale - 3.58571
        }
    }

    Row {
        anchors.fill: parent
        anchors.rightMargin: 15
        anchors.bottomMargin: 8
        anchors.leftMargin: -15
        anchors.topMargin: 12
        spacing: 38

        Item {
            width: 212
            height: 602
            Image {
                x: 210
                y: -206
                width: 212
                height: 602
                source: "qrc:/images/static-graph.png"
                rotation: 90
            }
        }

        Item {
            width: 382
            height: 602

            Image {
                id: dots
                x: 330
                width: 8
                height: 32
                source: "qrc:/images/change-view.png"
                rotation: 90
                anchors.top: parent.top
                anchors.topMargin: 216
            }

            Item {
                width: 300
                height: 345
                anchors.top: dots.bottom
                anchors.topMargin: -84
                rotation: 90
                x: -13
                Image {
                    source: "qrc:/images/center.png"
                    anchors.centerIn: parent
                }
                Image {
                         id:  gogo
                         source: "qrc:/images/left.png"
                         anchors.verticalCenterOffset: 70
                         anchors.horizontalCenterOffset: 56
                         x: 90
                         y: 200
                         width: 254
                         height: 118
                         anchors.centerIn: parent
                         opacity: root.lungsOpacity
                         transform: Scale {
                         yScale: root.yScale
                         xScale: root.xScale
                         origin.x: gogo.implicitWidth / 2
                         origin.y: gogo.implicitHeight / 2
                     }
                }

                Image {
                    id: popo
                    x: 90
                    y: 20
                    source: "qrc:/images/right.png"
                    anchors.verticalCenterOffset: -80
                    anchors.horizontalCenterOffset: 57
                    anchors.centerIn: parent
                    opacity: root.lungsOpacity
                    transform: Scale {
                        yScale: root.yScale
                        xScale: root.xScale
                        origin.x: popo.implicitWidth / 2
                        origin.y: popo.implicitHeight / 2
                    }
                }
            }

            Image {
                x: -331
                y: 309
                source: "qrc:/images/extra-data.png"
                anchors.bottomMargin: 173
                rotation: 90
                anchors.bottom: parent.bottom
            }
        }
    }
}
