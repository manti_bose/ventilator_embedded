import QtQuick 2.1
import QtQuick.Controls.Material 2.1
import QtQuick.Controls 2.1
import QtQuick 2.0
Item {
    id: root
    width: 600
    height: 310

    property int ppeak: 28
    property real expMinVol: 5.1
    property int vte: 346
    property int fTotal: 15

    Timer {
        running: true
        repeat: true
        interval: 500
        onTriggered: {
            var r = Math.floor(Math.random() * 4)
            if (r === 0) {
                r = Math.floor(Math.random() * 3) - 1
                ppeak = ppeak +  r
            } else if(r === 1) {
            } else if(r === 2) {
                r = Math.floor(Math.random() * 3) - 1
                vte = vte +  r
            } else if(r === 3) {
                r = Math.floor(Math.random() * 3) - 1
                fTotal = fTotal +  r
            }
        }
    }

    Row {
        spacing: 2
        anchors.fill: parent
        rotation: 90
        anchors.rightMargin: -134
        anchors.bottomMargin: 142
        anchors.leftMargin: -134
        anchors.topMargin: 148
        Image {
            source: "qrc:/images/ppeak.png"
            anchors.bottom: parent.bottom

            Text {
                id:ppeakText
                text: root.ppeak.toString()
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 15
                anchors.leftMargin: 10
                font.pixelSize: 58
                rotation: -90
                color: "white"
                transform: [
                    Rotation {
                        origin.x: ppeakText.width / 2
                        origin.y: ppeakText.height / 2
                        angle: 0
                    }
                ]
            }
        }

        Image {
            source: "qrc:/images/expminvol.png"
            anchors.bottom: parent.bottom

            Text {
                id:expText
                text: root.expMinVol.toString()
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 15
                anchors.leftMargin: 10
                font.pixelSize: 58
                rotation: -90
                color: "white"
                transform: [
                    Rotation {
                        origin.x: expText.width / 2
                        origin.y: expText.height / 2
                        angle: 0
                    }
                ]
            }
        }

        Image {
            source: "qrc:/images/vte.png"
            anchors.bottom: parent.bottom
            Text {
                id: vteText
                text: root.vte.toString()
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 30
                anchors.leftMargin: 10
                font.pixelSize: 58
                rotation: -90
                color: "white"
                transform: [
                    Rotation {
                        origin.x: vteText.width / 2
                        origin.y: vteText.height / 2
                        angle: 0
                    }
                ]
            }
        }
        Image {
            source: "qrc:/images/ftotal.png"
            anchors.bottom: parent.bottom
            Text {
                id:ftotalText
                text: root.fTotal.toString()
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 15
                anchors.leftMargin: 10
                font.pixelSize: 58
                rotation: -90
                color: "white"
                transform: [
                    Rotation {
                        origin.x: ftotalText.width / 2
                        origin.y: ftotalText.height / 2
                        angle: 0
                    }
                ]
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.5;height:600;width:310}
}
##^##*/
